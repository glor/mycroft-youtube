requests>=1.1
python-vlc>=3.0.6109
pafy>=0.5.4
urllib3>=1.25.3
pyglet>=1.4.1
lxml>=4.3.4